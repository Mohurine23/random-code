function delay(time) {
    return new Promise(resolve => setTimeout(resolve, time));
}

function genHexString(len){
    const hex = '0123456789ABCDEF';
    let output = '';
    for (let i = 0; i < len; ++i) {
        output += hex.charAt(Math.floor(Math.random() * hex.length));
    }
    return output;
}

var isTrue = 1;

function reload(){
    isTrue = 0;
    document.getElementById("content0").textContent = "";
    random_go();
}

function do_stuff(){
    if(isTrue == 1){
        document.getElementById("title0").textContent = genHexString(30);
        document.getElementById("content0").textContent = document.getElementById("content0").textContent + genHexString(4);
        document.getElementById("content0").style.color = ("#"+genHexString(6));
        document.getElementById("background0").style.backgroundColor = ("#"+genHexString(6));
        random_go();
    }
}

function random_go(){
    isTrue = 1;
    delay(10).then(() => do_stuff());
    delay(5000).then(() => reload());
}